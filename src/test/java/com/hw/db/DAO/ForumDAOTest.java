package com.hw.db.DAO;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import java.util.Collections;
import java.util.List;


class ForumDAOTest {

    //test from lab
    @Test
    @org.junit.jupiter.api.DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        Mockito.verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    //hw full branch coverage tests
    private List<Object> emptyArray;

    @org.junit.jupiter.api.BeforeEach
    @org.junit.jupiter.api.DisplayName("stubs for testing")
    void createStubs() {
        emptyArray = Collections.emptyList();

    }

    @Test
    void userListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);

        assertEquals(emptyArray, ForumDAO.UserList("some", 10,"1", true));
    }

    @Test
    void userListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);

        assertEquals(emptyArray, ForumDAO.UserList("some", 10,"1", true));
    }

    @Test
    void userListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);

        assertEquals(emptyArray, ForumDAO.UserList("some", null,null, null));
    }
}