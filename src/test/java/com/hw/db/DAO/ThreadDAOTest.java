package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ThreadDAOTest {

    private List<Object> emptyArray;

    @org.junit.jupiter.api.BeforeEach
    @org.junit.jupiter.api.DisplayName("stubs for testing")
    void createStubs() {
        emptyArray = Collections.emptyList();
    }

    @Test
    void treeSort1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO forum = new ThreadDAO(mockJdbc);

        assertEquals(emptyArray, ThreadDAO.treeSort(1, 1,1, true));
    }

    @Test
    void treeSort2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO forum = new ThreadDAO(mockJdbc);

        assertEquals(emptyArray, ThreadDAO.treeSort(1, 1,1, false));
    }

}