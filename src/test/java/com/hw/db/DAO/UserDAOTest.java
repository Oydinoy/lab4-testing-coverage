package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class UserDAOTest {

    private User testUser;

    @Test
    void changeTest1() {
        testUser = new User(
                "Berdina",
                "email.com",
                null,
                null
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;",
                    testUser.getEmail(),  testUser.getNickname());

        }
    }

    @Test
    void changeTest2() {
        testUser = new User(
                "Berdina",
                null,
                "Oydinoy",
                null
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;",
                    testUser.getFullname(),  testUser.getNickname());

        }
    }

    @Test
    void changeTest3() {
        testUser = new User(
                "Berdina",
                null,
                null,
                "Am writing test cases"
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;",
                    testUser.getAbout(),  testUser.getNickname());

        }
    }

    @Test
    void changeTest4() {
        testUser = new User(
                "Berdina",
                null,
                null,
                null
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());

        }
    }

    @Test
    void changeTest5() {
        testUser = new User(
                "Berdina",
                "email.com",
                "Oydi",
                null
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;",
                    testUser.getEmail(), testUser.getFullname(),  testUser.getNickname());
        }
    }

    @Test
    void changeTest6() {
        testUser = new User(
                "Berdina",
                "email.com",
                null,
                "writing tests"
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;",
                    testUser.getEmail(), testUser.getAbout(),  testUser.getNickname());
        }
    }

    @Test
    void changeTest7() {
        testUser = new User(
                "Berdina",
                null,
                "Oydi",
                "writing tests"
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;",
                    testUser.getFullname(), testUser.getAbout(),  testUser.getNickname());
        }
    }

    @Test
    void changeTest8() {
        testUser = new User(
                "Berdina",
                "email.com",
                "Oydi",
                "writing tests"
        );

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userdao = new UserDAO(mockJdbc);

            UserDAO.Change(testUser);
            Mockito.verify(mockJdbc).update("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;",
                    testUser.getEmail(), testUser.getFullname(), testUser.getAbout(),  testUser.getNickname());
        }
    }
}